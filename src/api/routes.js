import { Router } from 'express';
import apiResourceHandler from '../api/resource/handler';
import routes from '../../routes.json';

export default ({ config }) => {

  function addRoute(api, name, routes) {
      api.use(routes, function(req, res, next) {
        new apiResourceHandler(name, req, res);
      });
  }

  var api = Router({
    mergeParams: true
  });

  console.log('Loading routes: ');

  // add routes from routes.json
  for (var key in routes) {
    console.log(routes[key]['routes']);
    addRoute(api, key, routes[key]['routes']);
  }

  return api;
}
