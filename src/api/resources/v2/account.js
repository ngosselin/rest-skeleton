import apiResourceV2 from '../../../api/resources/v2';

export default class apiAccountResourceV2 extends apiResourceV2 {

  // GET
  index() {
    // no :id
    if (typeof this.request.params.id === 'undefined') {
      return this.response.status(200)
        .send(
          JSON.stringify(
            {
              'id': null,
              'message': 'valid user'
            }
          )
        );
    }

    // :id
    if (typeof this.request.params.id !== 'undefined') {
      return this.response.status(200)
        .send(
          JSON.stringify(
            {
              'id': this.request.params.id,
              'message': 'valid user'
            }
          )
        );
    }
  }

  // POST
  create() {
    if (Object.keys(this.request.body).length > 0) {
      return this.response.status(200)
        .send(
          JSON.stringify(
            this.request.body
          )
        );
    }
    return this.response.status(500)
      .send(
        JSON.stringify(
          {
            'error': true,
            'message': 'PUT request, missing parameters'
          }
        )
      );
  }

  // PUT
  update() {
    if (this.request.params.id && this.request.query.name) {
      return this.response.status(200)
        .send(
          JSON.stringify(
            {
              'id' : this.request.params.id,
              'name' : this.request.query.name
            }
          )
        );
    }
    return this.response.status(500)
      .send(
        JSON.stringify(
          {
            'error': true,
            'message': 'PUT request, missing parameters'
          }
        )
      );
  }

  // DELETE
  destroy() {
    if (this.request.params.id) {
      return this.response.status(200)
        .send(
          JSON.stringify(
            {
             'id': this.request.params.id, 
             'message': 'deleted'
            }
          )
        );
    }
    return this.response.status(500)
      .send(
        JSON.stringify(
          {
            'error' : true,
            'message' : 'missing id parameter'
          }
        )
      );
  }
}
