import apiResource from '../../api/resource';

export default class apiResourceV2 extends apiResource {
  constructor(request, response) {
    super(request, response);
    this.response.set('api-version', '2');
  }
}
