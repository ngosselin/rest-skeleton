export default class apiResource {
  constructor(request, response) {
    this.request = request;
    this.response = response;

    this.response.setHeader('content-type', 'application/json');
  }
}
