export default class apiResourceFactory {

  getInstance(name, request, response) {
    let instance = null;

    try {
      let obj = require('../../api/resources/' + name);

      try {
        return new obj.default(request, response);
      } catch (exception) {
        console.log('apiResourceFactory unable to create resource object.');
      }

    } catch (exception) {
      console.log('apiResourceFactory unable to require resource.');
    }

    return null;
  }
}
