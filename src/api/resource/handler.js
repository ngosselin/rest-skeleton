import apiResourceFactory from '../../api/resource/factory';

export default class apiResourceHandler {
  constructor(name, request, response) {
    this.name = name;
    this.request = request;
    this.response = response;
    this.factory = new apiResourceFactory();
    this.resourceHandler = null;

    // handle the request
    this.handleRequest();

  }

  createResourceHandler(name) {
    this.resourceHandler = this.factory.getInstance(name, this.request, this.response);
  }

  handleRequest() {
    this.createResourceHandler(this.name);

    if (this.resourceHandler !== null) {
      switch (this.request.method) {
        case 'GET':
          this.resourceHandler.index();
          break;
        case 'POST':
          this.resourceHandler.create();
          break;
        case 'PUT':
          this.resourceHandler.update();
          break;
        case 'DELETE':
          this.resourceHandler.destroy();
          break;
        default:
          console.log('Invalid Request Method Type: ' + this.request.method);
          break;
      }

      return;
    }

    console.log('resourceHandler is NULL');
    this.response.status(404)
      .send('resourceHandler is NULL.');
  }
}
