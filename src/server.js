import http from 'http';
import express from 'express';
import cors from 'cors';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import config from '../config.json';
import routes from './api/routes';

let app  = express();
app.server = http.createServer(app);
app.use(morgan('dev'));

// 3rd party middleware
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// router
app.use('/api', routes({config}));

// start server
app.server.listen(config.server.port);
console.log("\n" + `Listening on port: ${app.server.address().port}` + "\n");

export default app;
