## Description

rest-skeleton, is a Express (https://express.js.com) / Node.js (https://nodejs.org/en/) REST API boilerplate.

# Build the application
```
npm run build
```

# Run the tests
```
npm run test
```

# Start the application
```
npm run start
```

You can now use [http://127.0.0.1:8080](http://127.0.0.1:8080) to access the API.
