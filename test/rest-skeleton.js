let chai = require('chai');
let chaiHttp = require('chai-http');
let expect = chai.expect;
let should = chai.should;

let server = require('../src/server').default;

let apiVersion = 1;

let versions = [1, 2];

chai.use(chaiHttp);

describe('Task API Routes', function() {
  for (let i = 0; i < versions.length; i++) {
    describe('GET /api/v' + versions[i] + '/accounts', function() {
      it('should return JSON with null for id, and "valid user" for message.', function(done) {
        let expectedJSON = {
          'id': null,
          'message': 'valid user',
        };

        chai.request(server)
          .get('/api/v' + versions[i] + '/accounts')
          .end(function (err, res) {
            expect(res).to.have.status(200);
            expect(res).to.have.headers;
            expect(res).to.have.header('api-version', versions[i]);
            expect(res.text).to.equal(JSON.stringify(expectedJSON));
            done();
          })
      });
    });

    describe('GET /api/v' + versions[i] + '/accounts/1', function() {
      it('should return JSON with "1" for id, and "valid user" for message.', function(done) {
        let expectedJSON = {
          'id': '1',
          'message': 'valid user',
        };
        chai.request(server)
          .get('/api/v' + versions[i] + '/accounts/1')
          .end(function (err, res) {
            expect(res).to.have.status(200);
            expect(res).to.have.headers;
            expect(res).to.have.header('api-version', versions[i]);
            expect(res.text).to.equal(JSON.stringify(expectedJSON));
            done();
          })
      });
    });

    describe('PUT /api/v' + versions[i] + '/accounts/1', function() {
      it('should return JSON with "1" for id, and "test" for name.', function(done) {
        let expectedJSON = {
          'id': '1',
          'name': 'test'
        };
        chai.request(server)
          .put('/api/v' + versions[i] + '/accounts/1')
          .query({name: 'test'})
          .end(function (err, res) {
            expect(res).to.have.status(200);
            expect(res).to.have.headers;
            expect(res).to.have.header('api-version', versions[i]);
            expect(res.text).to.equal(JSON.stringify(expectedJSON));
            done();
          })
      });
    });

    describe('PUT /api/v' + versions[i] + '/accounts/1', function() {
      it('should return 500.', function(done) {
        chai.request(server)
          .put('/api/v' + versions[i] + '/accounts/1')
          .end(function (err, res) {
            expect(res).to.have.status(500);
            expect(res).to.have.headers;
            expect(res).to.have.header('api-version', versions[i]);
            done();
          })
      });
    });

    describe('POST /api/v' + versions[i] + '/accounts', function() {
      it('should send json to : POST /api/v' + versions[i] + '/accounts', function(done) {
        let expectedJSON = {
          id: '1',
          name: 'test'
        };
        chai.request(server)
          .post('/api/v' + versions[i] + '/accounts')
          .send(expectedJSON)
          .end(function (err, res) {
            expect(res).to.have.status(200);
            expect(res).to.have.headers;
            expect(res).to.have.header('api-version', versions[i]);
            expect(res.text).to.equal(JSON.stringify(expectedJSON));
            done();
          })
      });
    });

    describe('POST /api/v' + versions[i] + '/accounts', function() {
      it('should POST to /api/v' + versions[i] + '/accounts and 500', function(done) {
        let expectedJSON = {
        };
        chai.request(server)
          .post('/api/v' + versions[i] + '/accounts')
          .send(expectedJSON)
          .end(function (err, res) {
            expect(res).to.have.status(500);
            expect(res).to.have.headers;
            expect(res).to.have.header('api-version', versions[i]);
            done();
          })
      });
    });

    describe('POST /api/v' + versions[i] + '/accounts', function() {
      it('should POST to /api/v' + versions[i] + '/accounts and 500', function(done) {
        chai.request(server)
          .post('/api/v' + versions[i] + '/accounts')
          .end(function (err, res) {
            expect(res).to.have.status(500);
            expect(res).to.have.headers;
            expect(res).to.have.header('api-version', versions[i]);
            done();
          })
      });
    });

    describe('DEL /api/v' + versions[i] + '/accounts/1', function() {
      it('should send json to : DEL /api/v' + versions[i] + '/accounts/1', function(done) {
        let expectedJSON = {
          id: '1',
          message: 'deleted'
        };
        chai.request(server)
          .delete('/api/v' + versions[i] + '/accounts/1')
          .send(expectedJSON)
          .end(function (err, res) {
            expect(res).to.have.status(200);
            expect(res).to.have.headers;
            expect(res).to.have.header('api-version', versions[i]);
            expect(res.text).to.equal(JSON.stringify(expectedJSON));
            done();
          })
      });
    });

    describe('DEL /api/v' + versions[i] + '/accounts', function() {
      it('should DEL to /api/v' + versions[i] + '/accounts and 500', function(done) {
        chai.request(server)
          .delete('/api/v' + versions[i] + '/accounts')
          .end(function (err, res) {
            expect(res).to.have.status(500);
            expect(res).to.have.headers;
            expect(res).to.have.header('api-version', versions[i]);
            done();
          })
      });
    });
  }
});
